const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const fetch = require('node-fetch');
const fs = require('fs')
// axios

const listUrl = 'http://callumacrae.github.io/regex-tuesday/';

async function fetchList() {
    const res = await fetch(listUrl)
    const text = await res.text();

    const dom = new JSDOM(text);
    const aList = Array.from(dom.window.document.querySelectorAll('.challenges a'));
    const testList = aList.map(a => ({
        link: 'http://callumacrae.github.io/regex-tuesday/' + a.href,
        name: a.text.split('-')[1].trim()
    }));

    let testDataList = await Promise.all(testList
        //.filter((t, i) => i == 0)
        .map(async (testInfo, i) => {
            const res = await fetch(testInfo.link)
            const text = await res.text();

            const dom = new JSDOM(text);
            const doc = dom.window.document;
            const section = doc.querySelector('section');

            if(doc.querySelector('#replace'))
                return null;

            const input = section.querySelector('.single-input');
            let sibling = input.previousElementSibling;
            const descriptionParagraphs = [];
            while (sibling) {
                descriptionParagraphs.push(sibling.outerHTML);
                sibling = sibling.previousElementSibling;
            }
            descriptionParagraphs.reverse();
            const description = descriptionParagraphs.join('');

            const testCases = Array.from(doc.querySelector('#tests')
                .querySelectorAll('dt'))
                .map(dt => {
                    const m = dt.nextElementSibling.innerHTML;
                    return {
                        value: dt.innerHTML,
                        assert: m == 'match' ? 'match' : 'unmatch'
                    }
                })
            return {
                id: i+1,
                title: testInfo.name,
                description: description,
                originalUrl: testInfo.link,
                testCases: testCases
            }
        }));

    testDataList = testDataList.filter(Boolean);

    testDataList.forEach(t=>{
        fs.writeFile(`./TestData/${t.id}.json`, JSON.stringify(t), err=>{
            console.error(err);
        })
    })
}

fetchList()